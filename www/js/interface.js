$(document).ready(function(){
	// placeholder
	$('input[data-placeholder], textarea[data-placeholder]').each(function() {
		var placeholder = $(this).attr('data-placeholder');
		if ((($(this).val() !== undefined) && ($(this).val().length > 0)) && ($(this).val() != placeholder)) {
			$(this).removeClass('placeholder');
		} else {
			$(this).val(placeholder);
			$(this).addClass('placeholder');
		}
		$(this).focusin(function() {
			$(this).removeClass('placeholder');
			if (($(this).val() === undefined) || ($(this).val() == placeholder)) {
				$(this).val('');
			}
		});
		$(this).focusout(function() {
			if (($(this).val() === undefined) || ($(this).val() == '') || ($(this).val() == placeholder)) {
				$(this).val(placeholder);
				$(this).addClass('placeholder');
			}
		});
	});

	/* HEADER */
	var FilialsMaxWidth = 0;
	$(".filials").each(function() {
		var FilialsWidth = $(this).width();
		if ( FilialsMaxWidth < FilialsWidth ) {
			FilialsMaxWidth = FilialsWidth;
		}
	});
	$(".filials").width(FilialsMaxWidth);
	/* END HEADER */
});
